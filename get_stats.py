import json
import requests
from pokemon import Pokemon
import time

def get_poke_stats(name):
    data = [-1, -1, -1, -1]
    try:
        start = time.time()#get our starting timestamp
        test = requests.get("https://pokeapi.co/api/v2/pokemon/" + name).json()["stats"]
        end = time.time()#get our end timestamp
        dif = end - start #invocation time
        print("Starting invocation at: ", start)
        print("Ending invocation at: ", end)
        print("Invocation time: ", dif)

    except:
        return None

    print(name)

    for i in range(len(test)):
        if test[i]["stat"]["name"] == "hp":
            data[0] = test[i]["base_stat"]#health

        elif test[i]["stat"]["name"] == "attack":
            data[1] = test[i]["base_stat"]#attack

        elif test[i]["stat"]["name"] == "defense":
            data[2] = test[i]["base_stat"]#defense

        elif test[i]["stat"]["name"] == "speed":
            data[3] = test[i]["base_stat"]#speed
        #we cannot put it together in a single if statement
        #as we must ensure the order of stats is always the same
    
    if (data[0] and data[1] and data[2] and data[3]):#check that all of our values are positive
        #if they are negative something is off
        #we need to ensure it is the same length every time
        our_pokemon = Pokemon(name, data[0], data[1], data[2], data[3])#make our pokemon and return
        return our_pokemon
    else:
        None #something has gone wrong


get_poke_stats('pikachu')