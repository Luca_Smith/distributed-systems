from flask import Flask
from flask_restful import Api, Resource,  reqparse
from pokemon import Pokemon
from get_stats import get_poke_stats
from connect_db import create_db
import random
import os
from flask_cors import CORS
import random

app = Flask(__name__)
CORS(app, resources={r"/api/*": {"origins": "*"}})
our_pokemon = ["charizard", "ditto", "gengar", "gible", "ivysaur", "jigglypuff", "lopunny", "pikachu", "psyduck", "snorlax", "squirtle"]

#NOTE: This application runs on the included environment folder .venv. All nercessary libraries
# are included inside of it. You can activate it through using the activate.bat file inside of the Scripts folder
# My machine does not allow the use of scripts, so I needed to use the following command
# I have written it down for my own benefit
# Set-ExecutionPolicy Unrestricted -Scope Proces
# .\.venv\Scripts\activate 

pokeList = []

@app.route("/api/<string:name>")
def pokemon(name):
    #our small web service
    inList = []
    for pokemon in our_pokemon:#returns a randmly chosen pokemon from a list, excluding the input name
        if pokemon != name:
            inList.append(pokemon)#add a pokemon to the list of available pokemon if it does not match the name
    randint = random.randint(0, len(inList)-1)
    chosen_pokemon = inList[randint]#choose a random pokemon
    return chosen_pokemon

if __name__ == "__main__":
    
    for pokemon in our_pokemon:#loop over out list of pokemon
        pokeStats = get_poke_stats(pokemon)#get the stats of every pokemon
        if(pokeStats):
            pokeList.append(pokeStats)#put them on a list
        else:#something has gone wrong
            print("Failed to fecth pokemon, exiting...")
            input("Press any key to exit.")
            os._exit(True)
    
    if (not create_db("10.41.96.140", "pal", "PASSWORD", pokeList)):#update/add to our external db, note, the IP will change
        print("Failed to connect to db, exiting...")#something has gone wrong, exit
        input("Press any key to exit.")
        os._exit(True)

    #everything looks good, start our API
    app.run(host='0.0.0.0')

