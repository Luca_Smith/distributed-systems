import mysql.connector
#from pokemon import Pokemon

def create_db(host_name, user_name, password, data):
    try :
        mydb = mysql.connector.connect(
        host=host_name,#this is the hidden ipv4 available by writing ipconfig on target computer's cmd terminal
        user=user_name,#our username and password
        password=password)
        cursor = mydb.cursor(buffered=True)#buffer our cursor to insert values into table

    except:
        return False
    
    cursor.execute('select Id from contactdb.pokemon')#get our Id's in the table, to make sure it is not empty or missing pokemon
    IdList = cursor.fetchall()#get our stuff
    IdList.reverse()#reverse the list so the last value is the first

    if(len(IdList) == 0):#if our list is empty, so is the table
        idInt = 0#start inserting from the beginning
    else:
        idInt = int(IdList[0][0])#else, get our last value

    Id = 1#will serve as our id
    for pokemon in data:#loop through our list of pokemon
        data = [pokemon.name, pokemon.hp, pokemon.attack, pokemon.defense, pokemon.speed, Id]#put all values into a list
        if (Id > idInt):
            cursor.execute('INSERT INTO contactdb.pokemon(PokeName, Hp, Attack, Defence, Speed, id) VALUES (%s,%s,%s,%s,%s,%s)', data)
            #if we exceed the boundries of the db, start inserting
        else:
            cursor.execute('UPDATE contactdb.pokemon SET PokeName = %s, Hp = %s, Attack = %s, Defence  = %s, Speed  = %s WHERE id = %s', data)
            #else update our things
        #then update the table with the values
        Id+= 1


    
    mydb.commit()
    cursor.close()
    return True#everything went smoothly



def deleteRow(host_name, user_name, password, ID): #function to delete a row in the database
    try :
        mydb = mysql.connector.connect(
        host=host_name,#this is the hidden ipv4 available by writing ipconfig on target computer's cmd terminal
        user=user_name,#our username and password
        password=password)
        cursor = mydb.cursor(buffered=True)#buffer our cursor to insert values into table

    except:
        return False


    cursor.execute("Delete from Laptop where id = %s", ID)#delete a specified row
    mydb.commit()
    cursor.close()
    return True#everything went smoothly